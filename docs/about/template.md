# Template

To get a uniform feel for the tasks and exercises, use this template.

Start by describing why one would want to know the stuff below


## Background

Do an [explanation](https://documentation.divio.com/explanation) about the key concepts.


## Getting started

What should you do to learn this?

Either

1. Link to how-to guides and tutorials from the internet

    And glue it together with appropriate text.

2. Write a tutorial or a how-to guide yourself.

Read more on [how-to guide](https://documentation.divio.com/how-to-guides/) and [tutorials](https://documentation.divio.com/tutorials/)


## other references

In order to go in-depth with the topic, [reference material](https://documentation.divio.com/reference/) is here

Either as a list or some text with links.


## Exercises

Do three levels of exercises

### Exercise 1: Knowledge

Simple exercise for the user who has read the above and might redo something simple on their own.

* Explain in your own words...

* Go through the applicaton, load the example and press run

This is to ensure that the user knows what it is about.


### Exercise 2: Basic skill

Do something.

* As a user, create something simple
* Use the more advanced features and explain what you did


### Exercise 3: Fluent

If you can do this exercise, you are at a level where you are self-correcting, ie. you know when you do something wrong and has a good idea about where to go for help.

Doing this easily means that you know the reference material and have been thorugh the guides and understands them.








