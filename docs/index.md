# Welcome to IT Docs

This is/will be a collection of documentation, so there is a reference available whenever the teacher asks something specific from students.

It is based on the philosophy, that we mus be able to provide relevant resources to the students whenever they are in doubt about e.g. an exercise.


The target audience are the teachers who wants easy consistent reference point to materials, but mostly for the college level students that are interested in getting startet with the topics.

## Examples

### Write a nice report

Some student are supprised at what is expected from a report, and how modern word processor tools may help them.

Conclusion is to add a description of how moderne wordprocessors work with headings, table of content, page numbering and such. This will include links to relevant resources about MS word, libreoffice, LaTeX, or whatever other word processor we find relevant.


### Spin up a VM with linux

This requires some background on VMs and hypervisors, and links to guide for the most common ones.

It also requires some background on linux and the installation procedure, and perhaps something about pre-installed images and their use.
